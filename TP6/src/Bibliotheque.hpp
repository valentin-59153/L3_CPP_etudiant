#ifndef BIBLIOTHEQUE_HPP
#define BIBLIOTHEQUE_HPP
#include <vector>
#include "Livre.hpp"


class Bibliotheque : public std::vector<Livre>
{
public:
    Bibliotheque();
    void afficher()const;
    void trierParAuteurEtTitre();
    void trierParAnnee();
    void ecrireFichier(const std::string & nomFichier) const;

};

#endif // BIBLIOTHEQUE_HPP
