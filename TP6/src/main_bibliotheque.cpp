#include "Livre.hpp"
#include <iostream>
#include "Bibliotheque.hpp"

int main() {
    Bibliotheque b;
    b.push_back((Livre("La fleur rose","un homme",2)));
    b.push_back((Livre("L'eau bleu","une femme",3)));
    b.push_back((Livre("L'avion qui vole","un enfant",1)));
    b.afficher();
    b.trierParAnnee();
    b.afficher();
    b.ecrireFichier("testttt.txt");



    return 0;
}

