#include "Bibliotheque.hpp"
#include <iostream>
#include <vector>
#include <algorithm>
#include <fstream>

Bibliotheque::Bibliotheque()
{

}

void Bibliotheque::afficher() const
{
    for(std::vector<Livre>::const_iterator it=begin();it!=end();++it){
        std::cout<<*it;
    }
}
void Bibliotheque::trierParAuteurEtTitre(){
   std::sort(begin(),end());
}

void Bibliotheque::trierParAnnee(){
    auto cmp = [](const Livre & a, const Livre & b){
        return  a.getAnnee()<b.getAnnee();
    };
    sort(begin(),end());
}

void Bibliotheque::ecrireFichier(const std::string & nomFichier) const{
    std::ofstream ofs(nomFichier);
    for(const Livre & l : *this){
       ofs<<l;
    }

}
