#include "Livre.hpp"
#include <string>
#include <iostream>

Livre::Livre(){
    _titre="";
    _auteur="";
}

Livre::Livre(const std::string & titre,const std::string & auteur, int annee){
    std::size_t found1=titre.find("\n");
    std::size_t found2=titre.find(";");
    std::size_t found3=auteur.find("\n");
    std::size_t found4=auteur.find(";");
    if (found1!=std::string::npos ||
        found2!=std::string::npos ||
        found3!=std::string::npos ||
        found4!=std::string::npos){
        throw std::string("Caractere interdit!");
    }
    else{
        _titre= titre;
        _auteur=auteur;
        _annee=annee;
    }
}

const std::string & Livre::getTitre() const{
    return _titre;
}

const std::string & Livre::getAuteur() const{
    return _auteur;
}

int Livre::getAnnee() const {
    return _annee;
}

bool Livre::operator<(const Livre & l2) const {
    if(_auteur < l2._auteur) return true;
        else return _auteur == l2._auteur && _titre < l2._titre;
}
bool operator==(const Livre &l1, const Livre &l2){
    return l1.getAuteur()==l2.getAuteur() && l1.getTitre()==l2.getTitre() && l1.getAnnee() == l2.getAnnee();

}

void operator<<(std::ostream &os,const Livre & l){
   os << l.getTitre()<<";"<<l.getAuteur()<<";"<<l.getAnnee()<<std::endl;
};
