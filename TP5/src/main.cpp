#include "Point.hpp"
#include "Couleur.hpp"
#include "Ligne.hpp"
#include "FigureGeometrique.hpp"
#include "Polygoneregulier.hpp"
#include <iostream>
#include <vector>

int main() {
    Couleur c;
    c._r=0;
    c._g=1;
    c._b=0;

    Point p0;
    p0._x=150;
    p0._y=250;


    Point p1;
    p1._x=100;
    p1._y=200;

    Polygoneregulier pr0(c,p1,20,3);
    Polygoneregulier pr1(c,p1,50,5);
    Polygoneregulier pr2(c,p1,30,1);
    Polygoneregulier pr3(c,p1,60,2);
    Ligne l0(c,p0,p1);
    Ligne l1(c,p0,p1);
    Ligne l2(c,p0,p1);
    Ligne l3(c,p0,p1);

  /*  std::vector<FigureGeometrique> vec;

    vec.push_back(pr0);
    vec.push_back(l3);
    vec.push_back(pr1);
    vec.push_back(l2);
    vec.push_back(pr2);
    vec.push_back(l1);
    vec.push_back(pr3);
    vec.push_back(l0);

    for(unsigned int i=0;i<8;i++){
        vec[i].afficher();
    }*/


    return 0;
}

