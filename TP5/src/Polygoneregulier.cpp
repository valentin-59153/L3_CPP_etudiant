#include "Polygoneregulier.hpp"
#include <iostream>
#include <cmath>

Polygoneregulier::Polygoneregulier(Couleur const &couleur,Point const &centre,int rayon, int nbCotes) :FigureGeometrique(couleur)
{
   _Point=new Point[nbCotes];
   for(int i=0;i<nbCotes;i++){
      float ALPHA= i * M_PI * 2/nbCotes;
      int x= rayon * cos(ALPHA) + centre._x;
      int y= rayon * sin(ALPHA) + centre._y;
       _Point[i]= {x,y};
   }
   _nbPoints=nbCotes;
}

 void Polygoneregulier::afficher(){
    std::cout<<"PolygoneRegulier "<<getCouleur()._r<<"_"<<getCouleur()._g<<"_"<<getCouleur()._b<<" ";
    for(int i=0;i<_nbPoints;i++){
        std::cout<<_Point[i]._x<<"_"<<_Point[i]._y<<" ";
    }
    std::cout<<std::endl;
}

 int Polygoneregulier::getNbPoints(){
    return _nbPoints;
}

const Point &Polygoneregulier::getPoint(int indice){
    return _Point[indice];
}
