#ifndef LIGNE_HPP
#define LIGNE_HPP
#include "Point.hpp"
#include "FigureGeometrique.hpp"



class Ligne : public FigureGeometrique{
private:
    Point _p0;
    Point _p1;
public:
    Ligne(Couleur const &couleur,Point const &p0, Point const &p1);
    void afficher();
    const Point &getP0();
    const Point &getP1();
};

#endif // LIGNE_HPP
