#include "FigureGeometrique.hpp"
#include <iostream>

FigureGeometrique::FigureGeometrique(Couleur const &couleur ){
   _couleur=couleur;
}

const Couleur & FigureGeometrique::getCouleur(){
    return _couleur;
}

void FigureGeometrique::afficher(){
    std::cout<<"Figure Geometrique"<<std::endl;
}
