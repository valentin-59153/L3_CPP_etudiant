#include "Image.hpp"
#include <fstream>
#include <cmath>


Image::Image(int largeur,int hauteur){
   _largeur= largeur;
   _hauteur=hauteur;
   _pixels=new int [_largeur*_hauteur];
   for (int k=0;k<_largeur*_hauteur;k++){
       _pixels[k]=0;
   }
}

Image::~Image(){
    delete [] _pixels;
}

int Image::getLargeur() const{
    return _largeur;
}
int Image::getHauteur() const{
    return _hauteur;
}
int Image::getPixel(int i,int j) const{
    return _pixels[i*_largeur+j];
}
void Image::setPixel(int i, int j, int couleur){
    _pixels[i*_largeur+j]=couleur;
}
int Image::pixel(int i,int j )const{
    return _pixels[i*_largeur+j];
}
int & Image::pixel(int i, int j){
    return _pixels[i*_largeur+j];
}

void ecrirePnm(const Image & img, const std::string & nomFichier){
    std::ofstream ofs(nomFichier);
    ofs<<"P2"<< std::endl;
    ofs<<img.getLargeur()<<" "<< img.getHauteur()<<std::endl;
    ofs<<"255"<<std::endl;
    for(int i=0;i<img.getHauteur();i++){
        for (int j=0; j<img.getLargeur();j++){
            ofs << img.pixel(i,j);
        }
        ofs<<std::endl;
    }
}

void remplir(Image & img){
    for(int i=0;i<img.getHauteur();i++){
        int c=int(2*M_PI*i/img.getLargeur());
        for (int j=0; j<img.getLargeur();j++){
            img.pixel(j,i)=c;
        }
    }
}

Image bordure(const Image & img,int couleur){
    Image img2 = img;
    const int i1 = img2.getLargeur() -1;
                  for(int i = 0 ; i< img2.getHauteur();i++){
                      img2.pixel(i,0)=couleur;
                      img2.pixel(i,i1)=couleur;
                  }
   const int i2 = img2.getHauteur() -1;
                  for(int i = 0 ; i< img2.getLargeur();i++){
                      img2.pixel(i,0)=couleur;
                      img2.pixel(i,i2)=couleur;
                  }

}
