#ifndef FIGUREGEOMETRIQUE_HPP_
#define FIGUREGEOMETRIQUE_HPP_
#include "Couleur.hpp"

class FigureGeometrique{
    private:
		Couleur _couleur;
	public:
        FigureGeometrique(Couleur const &couleur );
		const Couleur & getCouleur();
        virtual void afficher();
	
};	

#endif
