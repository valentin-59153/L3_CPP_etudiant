#ifndef POLYGONEREGULIER_HPP
#define POLYGONEREGULIER_HPP
#include "Point.hpp"
#include "FigureGeometrique.hpp"

class Polygoneregulier : public FigureGeometrique{
private:
    int _nbPoints;
    Point * _Point;
public:
    Polygoneregulier(Couleur const &couleur,Point const &centre,int Rayon, int nbCotes);
    void afficher();
    int getNbPoints();
    const Point &getPoint(int indice);
};

#endif // POLYGONEREGULIER_HPP
