#include "ViewerFigures.hpp"
#include "ZoneDessin.hpp"

ViewerFigures::ViewerFigures(int argc, char ** argv) {
    _kit = Gtk::Main(argc, argv);

    _window.set_title("Bonjour");
    _window.set_default_size(640,480);
    ZoneDessin dessin;
    _window.add(dessin);
    _window.show_all();

}
void ViewerFigures::run() {
    _kit.run(_window);
}
