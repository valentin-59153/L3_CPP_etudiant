#ifndef ZONEDESSIN_HPP
#define ZONEDESSIN_HPP
#include <vector>
#include "FigureGeometrique.hpp"
#include <gtkmm.h>
#include <gdkmm.h>


class ZoneDessin: public Gtk::DrawingArea
{
    private:
        std::vector<FigureGeometrique*> _figures;
    public:
        ZoneDessin();
        bool on_expose_event(GdkEventExpose* event);
        bool gererClic(GdkEventButton* event);
        bool on_draw(const Cairo::RefPtr<Cairo::Context> &) override;

};

#endif // ZONEDESSIN_HPP

