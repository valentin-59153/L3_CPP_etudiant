#include "ZoneDessin.hpp"
#include <gtkmm.h>
#include "Ligne.hpp"
#include "Polygoneregulier.hpp"

ZoneDessin::ZoneDessin(){

    _figures.push_back(new Ligne({1,0,0}, {10,10}, {630,10}));
    _figures.push_back(new Ligne({1,0,0}, {630,10}, {630,470}));
    _figures.push_back(new Ligne({1,0,0}, {10,470}, {630,470}));
    _figures.push_back(new Ligne({1,0,0}, {10,10}, {10,470}));
    _figures.push_back(new Polygoneregulier({0,1,0}, {100,200}, 50, 5));

}
bool  ZoneDessin::on_expose_event(GdkEventExpose* event){

    return true;
}
bool  ZoneDessin::gererClic(GdkEventButton* event){
    return true;
}

bool ZoneDessin::on_draw(
    const Cairo::RefPtr<Cairo::Context> & context) {

    // règle le tracé
    context->set_source_rgb(1.0, 0.0, 0.0);
    context->set_line_width(10.0);

    // dessine une diagonale
    auto window = get_window();
    context->move_to(0, 0);
    context->line_to(window->get_width(),
                     window->get_height());

    // met à jour l'affichage
    context->stroke();

    return true;
}
